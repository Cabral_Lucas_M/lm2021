#include <stdio.h>
#include <stdint.h>
#include <string.h>

extern void EncenderLed(uint8_t led);
extern void ApagarLed(uint8_t led);
extern bool PresionBotonLogin(void);
extern bool CompararClave(char* usuario, int32_t N_usuario, char* clave, int32_t N_clave); //usuario y clave son punteros al primer caracter, N es la cantidad de caracteres de cada uno
extern int32_t ContadorIncorrectas(char* usuario, int32_t N_usuario);
extern void ResetIncorrectas(char* usuario, int32_t N_usuario);
extern void ImprimirenPantalla(string msj);
extern uint32_t cuenta_ms;