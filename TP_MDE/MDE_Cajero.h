#include <stdio.h>
#include <conio.h>
#include "drivers.h"

#define TIEMPO_ENCENDIDO 500
#define TIEMPO_APAGADO 500

class MDE_Cajero {

public:

    MDE_Cajero()
    { 
       init();
    }

    ~MDE_Cajero()
    {
        delete []usuario;
        delete []clave;
    }

    void init(void) {
        Estado = APAGADO;
        ApagarLed(LED_1);
        ApagarLed(LED_2);
        cuenta_caracteres = 0;
        caracteres_usuario = 0;
        caracteres_clave = 0;
    }

    void fsm()  {
        switch(Estado) {
            case APAGADO:
            {
                if(PresionBotonLogin) {
                    Estado = ESPERANDO_USUARIO;
                    EncenderLed(LED_2);
                    cuenta_ms = 0;
                    parpadeoLed2 = ENCENDIDO;
                    cuenta_caracteres = 0;
                }
            }

            case ESPERANDO_USUARIO:
            {
                if(cuenta_ms >= TIEMPO_ENCENDIDO && parpadeoLed2) {
                    ApagarLed(LED_2);
                    cuenta_ms = 0;
                    parpadeoLed2 = APAGADO;
                }
                
                if(cuenta_ms >= TIEMPO_APAGADO && !parpadeoLed2) {
                    EncenderLed(LED_2);
                    cuenta_ms = 0;
                    parpadeoLed2 = ENCENDIDO;
                }

                if(_kbhit()){                     //kbhit devuelve 1 si hay un caracter en el buffer de entrada por teclado
                    char c = _getch();            //getch() casi no interrumpe el programa, porque lee instantaneamente el buffer
                    if(c == 13){
                        Estado = ESPERANDO_CLAVE;
                        caracteres_usuario = cuenta_caracteres;
                    }
                    else{
                        usuario[cuenta_caracteres] = c;
                        cuenta_caracteres++;
                    }
                } 
                
            }                

            case ESPERANDO_CLAVE:
            {
                if(cuenta_ms >= TIEMPO_ENCENDIDO && parpadeoLed2) {
                    ApagarLed(LED_2);
                    cuenta_ms = 0;
                    parpadeoLed2 = APAGADO;
                }
                
                if(cuenta_ms >= TIEMPO_APAGADO && !parpadeoLed2) {
                    EncenderLed(LED_2);
                    cuenta_ms = 0;
                    parpadeoLed2 = ENCENDIDO;
                }

                if(_kbhit()){                     //kbhit devuelve 1 si hay un caracter en el buffer de entrada por teclado
                    char c = _getch();            //getch() casi no interrumpe el programa, porque lee instantaneamente el buffer
                    if(c == 13){
                        caracteres_clave = cuenta_caracteres;
                        if(CompararClave(usuario, caracteres_usuario, clave, caracteres_clave)){
                            Estado = SISTEMA_INICIADO;
                            EncenderLed(LED_1);
                            ApagarLed(LED_2);
                            cuenta_ms = 0;
                            parpadeoLed1 = ENCENDIDO;
                            parpadeoLed2 = APAGADO;
                        }
                        else if(ContadorIncorrectas(usuario, caracteres_usuario) >= 3){
                            Estado = SISTEMA_BLOQUEADO;
                            EncenderLed(LED_2);
                            parpadeoLed2 = ENCENDIDO;
                            break;
                        }
                        else{
                            Estado = APAGADO;
                            ApagarLed(LED_2);
                            parpadeoLed2 = APAGADO;
                        }
                    }
                    else{
                        clave[cuenta_caracteres] = getch();
                        cuenta_caracteres++;
                    }
                } 
            }  

            case SISTEMA_INICIADO:
            {
                if(cuenta_ms >= TIEMPO_ENCENDIDO && parpadeoLed1) {
                    ApagarLed(LED_1);
                    cuenta_ms = 0;
                    parpadeoLed1 = APAGADO;
                }
                
                if(cuenta_ms >= TIEMPO_APAGADO && !parpadeoLed1) {
                    EncenderLed(LED_1);
                    cuenta_ms = 0;
                    parpadeoLed1 = ENCENDIDO;
                }

                //Funciones a realizar del sistema...
            } 
            
            default:
                init();
        }
    }


private:
    enum estado {
        APAGADO,
        ENCENDIDO,
        ESPERANDO_USUARIO,
        ESPERANDO_CLAVE,
        SISTEMA_BLOQUEADO,
        SISTEMA_INICIADO
    };

    enum led {
        LED_1,
        LED_2
    };

    estado Estado, parpadeoLed1, parpadeoLed2;

    char* usuario = new char[50]; 
    char* clave = new char[50];
    int32_t cuenta_caracteres, caracteres_usuario, caracteres_clave;
    int32_t cont_incorrectas;

};