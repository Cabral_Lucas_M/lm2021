#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Ordenamiento.h"
#include "Ordenamiento.c"

void main (void)

{
    int32_t vector[10]={1,4,3,5,6,2,7,9,8,10};
    int32_t n=10;
    Ordenamiento(vector,n);
    for(int32_t i=0;i<n;i++)
    {
        printf("%d \n",vector[i]);
    }

    //Con llamado a swap por puntero>

    int32_t vector2[10]={11,19,12,13,18,15,16,14,17,20};
    Orden_puntero(vector2,n);
    for(int32_t i=0;i<n;i++)
    {
        printf("%d \n",vector2[i]);
    }
 
}