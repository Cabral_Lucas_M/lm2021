#include "Ordenamiento.h"

//////////////////////////////////////////////////////////////////////

static void (*pSwap)(int32_t *p1, int32_t *p2)= &swap;
static void swap(int32_t *p1, int32_t *p2)
{
int32_t aux;
aux=*p1;
*p1=*p2;
*p2=aux;
}

///// Quisimos hacer esta función en el archivo swap.c y usarla acá incluyendo el swap.h en ordenamiento.h pero nos dice que no está definida la función al hacer eso //////////////////////

static void Ordenamiento(int32_t *vector , int32_t n){

    int32_t i, j;
    for (i = 0; i < n-1; i++)   
    {
    for (j = 0; j < n-i-1; j++)

        if (vector[j] > vector[j+1])

            swap(&vector[j], &vector[j+1]);
    }

}

static void Orden_puntero(int32_t *vector, int32_t n){

     int32_t i, j;
    for (i = 0; i < n-1; i++)   
    {
    for (j = 0; j < n-i-1; j++)
        if (vector[j] > vector[j+1])
            (*pSwap)(&vector[j], &vector[j+1]);
    }

}