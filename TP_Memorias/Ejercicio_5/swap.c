#include "Swap.h"

static void (*pSwap)(int32_t *p1, int32_t *p2)= &swap;

static void swap(int32_t *p1, int32_t *p2)
{
  int32_t aux;
  aux=*p1;
  *p1=*p2;
  *p2=aux;
}