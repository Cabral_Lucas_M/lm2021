#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#define MaxCantDatos 100

struct colaDatos_t {
    short datos[MaxCantDatos];
    int32_t primero;
    int32_t ultimo;
    
}typedef ColaDatos_t;

ColaDatos_t *CrearcolaDatos (ColaDatos_t *c);

void AgregarDato (ColaDatos_t *Cola, short d);

short SacarDato (ColaDatos_t *Cola);

void ImprimirCola (ColaDatos_t *Cola);

bool ControlColaVacia (ColaDatos_t *Cola);

void main(void)
{

ColaDatos_t *Cola;
Cola= CrearcolaDatos (Cola);
AgregarDato(Cola,1);
AgregarDato(Cola,2);
AgregarDato(Cola,3);
AgregarDato(Cola,2);
ImprimirCola(Cola);
short ds= SacarDato(Cola);
printf("Sacando dato...%d\n",ds);
ImprimirCola(Cola);
if (ControlColaVacia(Cola)){
    printf("La cola esta vacia\n");
    }
free(Cola);
return;
}


ColaDatos_t *CrearcolaDatos (ColaDatos_t *Cola)
{
Cola= (ColaDatos_t*)malloc(sizeof(ColaDatos_t));
Cola->primero=0;
Cola->ultimo=-1;
return Cola;
}
void AgregarDato (ColaDatos_t *Cola, short d)
{  
 if (Cola->ultimo==(MaxCantDatos-1)){
     printf("No hay mas espacio\n");
 }
 else {
     Cola->ultimo++;
     Cola->datos[Cola->ultimo] = d;
      }
}
short SacarDato (ColaDatos_t *Cola){
if(ControlColaVacia(Cola))
printf("Cola vacia\n");
else{
    short dato= Cola->datos[Cola->primero];
    for(int32_t i=0;i<Cola->ultimo;i++)
    {
        Cola->datos[i]=Cola->datos[i+1];
    }
    Cola->ultimo=Cola->ultimo-1;
    return dato;
 }
}
void ImprimirCola (ColaDatos_t *Cola){
for (int32_t i=0; i<=Cola->ultimo; i++)
{
    printf("%d \n", Cola->datos[i]);
}
}
bool ControlColaVacia (ColaDatos_t *Cola){
    if(Cola->ultimo==-1)
    return true;
    else
    return false;
}