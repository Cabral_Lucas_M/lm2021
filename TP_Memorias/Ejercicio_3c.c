#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#define CantDatos 3
void AgregarDato(short d);
void LeerBuffer ();
struct buffer {
    short datos[CantDatos];
    int32_t u;
    bool cargando;
}typedef Buffer;
//Creo los 2 buffers
Buffer *ping;
Buffer *pong;

void main(void)
{
    ping->u = -1;
    pong->u = -1;
    ping->cargando = true;
    pong->cargando = false; //Por defecto se empieza cargando en ping y luego se pasa a pong
    AgregarDato(1);
    AgregarDato(2);
    AgregarDato(3);
    AgregarDato(4);
    AgregarDato(5);
    LeerBuffer();
    return;
}

void AgregarDato(short d)
{
    printf("Hola");
    if (ping->cargando == true)
    {  //Cargo datos en ping
        ping->u++;
        ping->datos[ping->u]=d;
        if(ping->u >= CantDatos-1)
        {
            ping->cargando = false;
            //Paso a cargar en pong
            pong->cargando=true;
        }
    }
    else
    {
    //Cargo datos en pong
        pong->u++;
        pong->datos[pong->u]=d;
        if(pong->u >= CantDatos-1)
        {
            pong->cargando = false;
            //Paso a cragar en ping
            ping->cargando = true;
        }
    }
}

void LeerBuffer ()//Esta disponible para leerse el que no se esta cargando.
//Al final de la lectura se pone de nuevo u en -1 
{ 
    printf("Hola");
    if(ping->cargando == false)
    { 
        if(ping->u == -1)
            printf("No hay datos\n");
        else
        { 
            for(int i = 0; i <= ping->u ; i++)
            printf("%d",ping->datos[i]);
            ping->u=-1;
        }
    }
    else
    {
        if(pong->u == -1)
            printf("No hay datos\n");
        else
        { 
            for(int i = 0; i <= pong->u; i++)
            printf("%d",pong->datos[i]);
            pong->u = -1;
        }
    }
}