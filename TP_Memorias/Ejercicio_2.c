#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int32_t Suma(int32_t a, int32_t b);
extern char etext, end;
int32_t varGlobalInit = 2;
int32_t varGlobalUninit;

void main(void)
{
    char ejercicio;
    int32_t varLocalInit = 1;
    int32_t varLocalUninit;
    int32_t (*pSuma) (int32_t,int32_t) = Suma;
    int32_t v1 = 2, v2 = 9;
    printf("Seleccione un inciso ('a', 'b', 'c', 'd'): ");
    scanf("%c",&ejercicio);
    getchar();

    switch (ejercicio)
    {
        case 'a':
        {
            printf("Direccion superior a memoria de programa:                   %p\n", &etext);
            printf("Direccion funcion Suma:                                     %p\n", pSuma);
            printf("Direccion del main:                                         %p\n", &main);    
            break;
        }

        case 'c':
        {
            printf("Direccion superior a memoria de datos NO inicializados:     %p\n", &end);
            printf("Direccion de variable Global inicializada:                  %p\n", &varGlobalInit);
            printf("Direccion de variable Local inicializada:                   %p\n", &varLocalInit);
            printf("Direccion de variable Global NO inicializada:               %p\n", &varGlobalUninit);
            printf("Direccion de variable Local NO inicializada:                %p\n", &varLocalUninit);               
            break;
        }

        case 'd':
        {
            printf("Direccion en memoria de parámetro 'v1':                      %p\n",&v1);
            printf("Direccion en memoria de parámetro 'v2':                      %p\n",&v2);              
            Suma(v1,v2);
            break;
        }

        default:
            break;
    }

    return 0;
}

int32_t Suma(int32_t a, int32_t b)
{
    printf("Direccion en memoria de parametro 'a':                        %p\n",&a);
    printf("Direccion en memoria de parametro 'b':                        %p\n",&b);   
    return 0;
}