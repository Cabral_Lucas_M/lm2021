#include "matematica.h"

int32_t Sumar_Array (int16_t* x, int16_t xn)
{
    int32_t suma = 0;
    for(int32_t i = 0; i < xn; i++)
        suma = suma + (int32_t)x[i];
    return suma;
}
