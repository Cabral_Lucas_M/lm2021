print("hola")

mi_var = 10
mi_flag = True

if mi_flag:
    print("flag : true")
    
    while mi_var > 0:
        print(mi_var)
        mi_var-=1

    print("Fin del if")

print("Fin script")

print("Tipos de datos:")

entero = 21
flotante = 3.11
cadena = "hola"

print(type(entero))
print(type(flotante))
print(type(mi_flag))
print(type(cadena))

if entero > 20 and flotante < 5 or cadena == "chau":
    print("condicion verdadera")
elif flotante == 3.11:
    print("condicion 2")
else:
    print("default")

print("strings")
msg0 = "hola mundo"
msg1 = 'hola mundo'

print(msg0)
print(msg1)

print("subindice 2")
print(msg0[2])

print("rango de letras")
print(msg1[3:8])
print(msg1[:7])
print(msg1[2:])
print(msg1[4:-1])

msg = "hola"
nombre = "juana"
dia = 15

print(msg + " " + nombre)
print(msg*2)

msg2 = "Hola %s. Hoy es %d de Junio" % (nombre, dia)

msg3 = "Hola {0}. Hoy es {1} de Junio". format(nombre, dia)

print(msg2)
print(msg3)

print("Hola " + nombre + ". Hoy es " + str(dia) + "de Junio")

print("Listas")

lista = [1,2,3,4]
print(lista)
print(lista[1])
print(lista[1:3])
print(lista[:3])

lista.append(5)
lista.append(6)
lista.remove(2)
print(lista)
print("Largo de la lista: " + str(len(lista)))

print("Tuplas") #Son invariables, no se puede usar ni append ni remove

tupla = (1,2,3,4)
print(tupla)
print(tupla[1])
print(tupla[1:3])
print(tupla[:3])

print(tupla)
print("Largo de la lista: " + str(len(lista)))

print("Diccionarios")

d = {"color":"rojo","estado":True,"id":22}
print(d)
print(d["estado"])
d["temp"] = 10.2
print(d)

print("Itero una lista")
for x in lista:
    print(x)

print("Itero una tupla")
for y in tupla:
    print(y)

print("Itero un rango")
for z in range(0,5):
    print(z)

print("Itero un dicc")
for k in d:
    print(k + ":" + str(d[k]))
