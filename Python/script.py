import numpy as np  #Es una librería que me permite manejar mejor un array respecto de las listas
import matplotlib.pyplot as plt  #Libreria para graficar
import pandas as pd  #Para manejo de datos

v_nat = [1,2,3]

print("lista nativa")
print(v_nat)
print(v_nat + v_nat)
print(v_nat *3)

v1 = np.array([1,2,3])
v2 = np.array([])

print("array numpy")
print(v1)
print(v2)
print("v1 + v1")
print(v1 + v1)
print(v1 * 3)
print(v1 * v1)

print(v1.tolist())
print(type(v1))

k2 = np.array(range(10))
print(k2)

fs = 500
ts = 1/fs

S1 = pd.read_csv("./presion_tp1.csv")
print(S1)

L = len(S1)
print("L = {0}".format(L))
k = np.arange(0,L)
t = k * ts

pf = abs(np.fft.fft(S1.values.reshape([1, L]).tolist()))

rf = fs/(L - 1)
f = k * rf

Vmax_sensor = 1
Vmin_sensor = -1

Vmax_adc = 3.3
Vmin_adc = 0

G = (Vmax_adc - Vmin_adc)/(Vmax_sensor - Vmin_sensor)
offset = (Vmax_adc - Vmin_adc)/2

S2 = S1 * G + offset

#-----
plt.subplot(2,1,1)
plt.plot(t, S1)
plt.grid()
plt.xlabel("t[s]")

plt.subplot(2,1,2)
plt.plot(t, S2)
plt.grid()
plt.xlabel("t[s]")

plt.show()

##################TP Python##################

S2_max = Vmax_adc
S2_min = Vmin_adc

#D1 (8 bits)
#def cuantificar(x,n):

N1 = 8 
D1_min = 0
D1_max = 2**N1 - 1
res_D1 = S2_max/D1_max
D1 = round(S2/res_D1)

plt.figure(3)
plt.title("D1 (8 bits)")
plt.subplot(2,1,1)
plt.plot(t,D1)
plt.grid()
plt.xlabel("t[s]")

##### Grafico en Frecuencia [w] ########
D1w = 20 * np.log10(abs(np.fft.fft(D1.values.flatten())))
plt.subplot(2,1,2)
plt.plot(D1w)
plt.grid()
plt.xlabel("w[]")
plt.show()

#D2 (12 bits)
N2 = 12 
D2_min = 0
D2_max = 2**N2 - 1
res_D2 = S2_max/D2_max
D2 = round(S2/res_D2)

plt.figure(4)
plt.title("D2 (12 bits)")
plt.subplot(2,1,1)
plt.plot(t,D2)
plt.grid()
plt.xlabel("t[s]")

##### Grafico en Frecuencia [w] ########
D2w = 20 * np.log10(abs(np.fft.fft(D2.values.flatten())))
plt.subplot(2,1,2)
plt.plot(D2w)
plt.grid()
plt.xlabel("w[]")
plt.show()

#D3 (24 bits)
N3 = 24
D3_min = 0
D3_max = 2**N3 - 1

res_D3 = S2_max /D3_max
D3 = round(S2/res_D3)

plt.figure(5)
plt.title("D3 (24 bits)")
plt.subplot(2,1,1)
plt.plot(t,D3)
plt.grid()
plt.xlabel("t[s]")

##### Grafico en Frecuencia [w] ########
D3w = 20 * np.log10(abs(np.fft.fft(D3.values.flatten())))
plt.subplot(2,1,2)
plt.plot(D3w)
plt.grid()
plt.xlabel("w[]")
plt.show()


##### Considero al error como la mezcla algebraica de la señal original y su digitalizacion, tanto en términos temporales como de frecuencia

E1 = D1*res_D1 - S2
E2 = D2*res_D2 - S2
E3 = D3*res_D3 - S2

plt.figure(6)
plt.title("ERROR")
plt.subplot(3,1,1)
plt.plot(t, E1)
plt.grid()
plt.xlabel("t[s]")
plt.subplot(3,1,2)
plt.plot(t, E2)
plt.grid()
plt.xlabel("t[s]")
plt.subplot(3,1,3)
plt.plot(t, E3)
plt.grid()
plt.xlabel("t[s]")
plt.show()

D1aux = D1*res_D1
D2aux = D2*res_D2
D3aux = D3*res_D3

S2w = 20 * np.log10(abs(np.fft.fft(S2.values.flatten())))
E1w = 20 * np.log10(abs(np.fft.fft(D1aux.values.flatten()))) - S2w
E2w = 20 * np.log10(abs(np.fft.fft(D2aux.values.flatten()))) - S2w
E3w = 20 * np.log10(abs(np.fft.fft(D3aux.values.flatten()))) - S2w


plt.figure(7)
plt.title("ERROR EN FRECUENCIA")
plt.subplot(3,1,1)
plt.plot(t, E1w)
plt.grid()
plt.xlabel("w[]")
plt.subplot(3,1,2)
plt.plot(t, E2w)
plt.grid()
plt.xlabel("w[]")
plt.subplot(3,1,3)
plt.plot(t, E3w)
plt.grid()
plt.xlabel("w[]")
plt.show()