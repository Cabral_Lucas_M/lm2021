def saludar():
    print("Hola")

def sumar(arg1, arg2 = 2):  #NO hace chequeo de datos, podría si quisiera meter un string (me va a tirar error). Hay que chequear manualmente.
    print("Suma de {0} + {1}".format(arg1,arg2))
    return arg1 + arg2